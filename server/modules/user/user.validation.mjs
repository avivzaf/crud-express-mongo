import Joi from "joi";
import raw from "../../middleware/route.async.wrapper.mjs";

export const userCreationSchema = Joi.object({
  first_name: Joi.string().alphanum().min(3).max(30).required(),

  last_name: Joi.string().alphanum().min(3).max(30).required(),

  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required(),

  phone: Joi.string().pattern(new RegExp("^[0-9]+$")).required().length(10),
});

export const createValidationMiddleware = raw(async (req, res, next) => {
  await userCreationSchema.validateAsync(req.body);
  next();
});

export const updateUserSchema = Joi.object({
  first_name: Joi.string().alphanum().min(3).max(30),

  last_name: Joi.string().alphanum().min(3).max(30),

  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),

  phone: Joi.string().pattern(new RegExp("^[0-9]+$")).length(10),
});

export const updateValidationMiddleware = raw(async (req, res, next) => {
  await updateUserSchema.validateAsync(req.body);
  next();
});
